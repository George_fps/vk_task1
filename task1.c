#include <stdio.h>
#include <stdlib.h>

int BinSearch(int x, int * ind, int *arr, int n);
int compare (const void * a, const void * b);

int main() {
	int n, *arr;
	freopen("task1.in", "r", stdin);
	freopen("task1.out", "w", stdout);
	
	scanf("%d", &n);
	
	arr = (int*) malloc(sizeof(int) * n);
	
	int i = 0;
	for (i = 0; i < n; ++i) {
		scanf("%d", &arr[i]);
	}
	
	qsort (arr, n, sizeof(int), compare);
	
	int ind = 0;
	i = 0;
	
	for (i = 0; i < 10; ++i)	{
		if (BinSearch(i, &ind, arr, n))	{
			printf("last %d found at index %d\n", i, ind - 1);
		}
		else {
			printf("%d not found\n", i);
		}
	}
	
	free(arr);
	return 0;
}

int BinSearch(int x, int * ind, int *arr, int n) {
	int l = 0, r = n, med = 0;
	
	if (n == 0) {
		return 0;
	}
	
	while (l < r - 1) {
		med = (r + l) >> 1;
    if (x >= arr[med]) {
      l = med;
		} else {
      r = med;
		}
  }

	if (arr[l] == x && r < n) {
		*ind = r;
		return 1;
	} else {
		return 0;
	}
}

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}
